import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AppComponent } from './app.component';
import { environment } from '../environments/environment';
import { RoutingModule } from './modules/routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ApplicationComponent } from './application/application.component';

import { MaterialModule } from './modules/material.module';
import { ButtonComponent } from './ui/button/button.component';
import { PageNotFoundComponent, PageNotFoundDialog } from './ui/page-not-found/page-not-found.component';
import { WhoAmIComponent } from './ui/who-am-i/who-am-i.component';
import { EducationComponent } from './ui/education/education.component';
import { ExperienceComponent } from './ui/experience/experience.component';
import { FormsModule } from '@angular/forms';
import { TopNavComponent } from './ui/top-nav/top-nav.component';
import { SideNavComponent } from './ui/side-nav/side-nav.component';
import { HttpClientModule } from '@angular/common/http';
@NgModule({
  declarations: [
    AppComponent,
    ApplicationComponent,
    ButtonComponent,
    PageNotFoundComponent,
    PageNotFoundDialog,
    WhoAmIComponent,
    EducationComponent,
    ExperienceComponent,
    TopNavComponent,
    SideNavComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    RoutingModule,
    FormsModule,
    HttpClientModule,
    MaterialModule
  ],
  entryComponents: [PageNotFoundDialog],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
