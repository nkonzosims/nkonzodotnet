import { Component, OnInit, ViewChild, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'ui-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit, OnChanges {

  @Input() disabled: boolean;
  @Input() autofocus = false;
  @Input() hasFocus: boolean;
  @ViewChild('btnElementValue') btnElementValue;

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    if (this.hasFocus) {
      setTimeout(() => {
       this.btnElementValue.nativeElement.focus();
      }
      , 0)
    }
  }

  ngOnChanges() {
    if (this.hasFocus) {
      setTimeout(() => {
       this.btnElementValue.nativeElement.focus();
      }
      , 0)
    }
  }
}
