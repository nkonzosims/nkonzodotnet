import { Component, OnInit, Input } from '@angular/core';
import { EducationModel } from './education.model';

@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.scss']
})
export class EducationComponent implements OnInit {

  @Input() data: EducationModel;
  constructor() { }

  ngOnInit() {
    this.data = { 
      certificateAttained: 'Matric Certificate',
      courseName: 'Grade 12',
      endDate: 'December 2013',
      startDate: 'January 2009',
      institutionName: 'Ephes Mamkeli Secondary'
    }
  }

  

}
