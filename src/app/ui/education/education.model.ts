export class EducationModel {

    constructor(
        public institutionName: string,
        public startDate: string,
        public certificateAttained: string,
        public endDate?: string,
        public courseName?: string,
    ) { }
}
