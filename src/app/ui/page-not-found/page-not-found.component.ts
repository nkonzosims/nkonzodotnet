import { Component, OnInit, Inject, AfterViewInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';

export interface DialogData {
  url: string;
}
const HOME_URL = '/';
@Component({
  selector: 'app-page-not-found',
  template: '<h1></h1>',
  styleUrls: ['./page-not-found.component.scss']
})
export class PageNotFoundComponent {

  currentUrl: string;

  constructor(public dialog: MatDialog,
              public router: Router) { this.openDialog() }

  openDialog(): void {
    const dialogRef = this.dialog.open(PageNotFoundDialog, {
      
      data: { url: this.router.url }
    });
  }

}

@Component({
  selector: 'not-found',
  templateUrl: './page-not-found.component.html',
  styles: []
})
export class PageNotFoundDialog implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<PageNotFoundDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public router: Router) { }

  ngOnInit() {
  }
  
  onHomeClicked() {
    this.dialogRef.close();
    this.router.navigate([HOME_URL], {skipLocationChange: false });
  }

  onStayClicked() {
    this.dialogRef.close();
  }
}
