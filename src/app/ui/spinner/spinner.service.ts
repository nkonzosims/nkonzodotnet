import { Injectable } from '@angular/core';

@Injectable()
export class SpinnerService {
    
    public isLoading = false;

    constructor() {}
    
    startLoading() {
        this.isLoading = true;
    }

    stopLoading() {
        this.isLoading = false;
    }
}