import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NavModel } from './nav.model';

@Component({
  selector: 'side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent implements OnInit {
  
  @Input() data: NavModel[];
  @Output() onNavClicked = new EventEmitter<NavModel>();

  constructor() { }

  ngOnInit() { }

  onNavClick(navItem: NavModel) {
    this.onNavClicked.emit(navItem);
  }

}
