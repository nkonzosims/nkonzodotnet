export class NavModel {
    constructor(
        public id: number,
        public icon: string,
        public name: string,
    ) { }
}
