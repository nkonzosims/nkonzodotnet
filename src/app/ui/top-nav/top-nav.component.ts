import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.scss']
})
export class TopNavComponent implements OnInit {

  @Output() onLogoClick = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  onLogoClicked(event) {
    console.log('Event ', event)
    this.onLogoClick.emit('onHomeClicked');
  }

  onItemClicked() {

  }
}
