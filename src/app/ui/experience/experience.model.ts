export class ExperienceModel {

    constructor(
    public startDate: string,
    public companyName: string,
    public jobTitle: string,
    public jobDescription: string,
    public reference: Reference,
    public endDate?: string,
    public imageURL?: string
    ) {}
}

interface Reference {
    name,
    email,
    contactNo?
}
