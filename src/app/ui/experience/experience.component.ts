import { Component, OnInit, Input } from '@angular/core';
import { ExperienceModel } from './experience.model';


@Component({
  selector: 'app-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.scss']
})
export class ExperienceComponent implements OnInit {

  @Input() data: ExperienceModel;

  public showRef = false;

  constructor() { }

  toggleReference() { 
    this.showRef = !this.showRef;
  }
  ngOnInit() {
    this.data = {
      companyName: 'Absa',
      jobTitle: 'Angular developer',
      startDate: '03 October 2017',
      endDate: 'TBA',
      jobDescription: 'Developing the user interface for Future dated payments and Prepaid Electricity list. Handling all the events and actions triggered within those entities. Error handling and responsive design',
      reference: {
        email: 'nkonzo.simelane@absa.co.za',
        contactNo: '079 999 7942',
        name: 'Nkonzo Simelane'
      }
    }
  }

}

