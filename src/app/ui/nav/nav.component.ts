import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  @Output() onLogoClick = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  onLogoClicked(event) {
    console.log('Event ', event)
    this.onLogoClick.emit('onHomeClicked');
  }

  onItemClicked() {

  }
}
