import { Injectable } from '@angular/core';
import { ExperienceModel } from '../ui/experience/experience.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

const URL = '';
const GET_ALL_EXPERIENCE = '';
@Injectable()
export class ExperienceService {

    constructor(private httpClient: HttpClient) { }

    public postExperience(experienceModel: ExperienceModel) : Observable<ExperienceModel> {
        return this.httpClient.post<ExperienceModel>(URL, experienceModel);
    }

    public getListOfExperience(): Observable<ExperienceModel> {
        return this.httpClient.get<ExperienceModel>(GET_ALL_EXPERIENCE);
    }
}
