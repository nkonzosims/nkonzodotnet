import { Component, OnInit } from '@angular/core';
import { NavModel } from '../ui/side-nav/nav.model';

@Component({
  selector: 'app-application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.scss']
})
export class ApplicationComponent implements OnInit {


  data = [{
    companyName: 'Absa',
    jobTitle: 'Angular developer',
    startDate: '03 October 2017',
    endDate: 'TBA',
    jobDescription: 'Developing the user interface for Future dated payments and Prepaid Electricity list. Handling all the events and actions triggered within those entities. Error handling and responsive design',
    reference: {
      email: 'nkonzo.simelane@absa.co.za',
      contactNo: '079 999 7942',
      name: 'Nkonzo Simelane'
    },
    imageURL: '../../../assets/icons/Absa-Logo.svg'
  },
  {
    companyName: 'Absa',
    jobTitle: 'Angular developer',
    startDate: '03 October 2017',
    endDate: 'TBA',
    jobDescription: 'Developing the user interface for Future dated payments and Prepaid Electricity list. Handling all the events and actions triggered within those entities. Error handling and responsive design',
    reference: {
      email: 'nkonzo.simelane@absa.co.za',
      contactNo: '079 999 7942',
      name: 'Nkonzo Simelane'
    },
    imageURL: '../../../assets/icons/Absa-Logo.svg'
  },
  {
    companyName: 'Absa',
    jobTitle: 'Angular developer',
    startDate: '03 October 2017',
    endDate: 'TBA',
    jobDescription: 'Developing the user interface for Future dated payments and Prepaid Electricity list. Handling all the events and actions triggered within those entities. Error handling and responsive design',
    reference: {
      email: 'nkonzo.simelane@absa.co.za',
      contactNo: '079 999 7942',
      name: 'Nkonzo Simelane'
    },
    imageURL: '../../../assets/icons/Absa-Logo.svg'
  }];

  sideNav: NavModel[] = [
    new NavModel(0, 'info', 'Mission Statement'),
    new NavModel(1, 'school', 'Education'),
    new NavModel(2, 'work', 'Experience'),
  ]

  public currentItemId = 0;

  constructor() {
  }

  ngOnInit() {
  }
  showPage(event: NavModel) {
    switch (event.id) {
      case 0:
        this.currentItemId = 0;
        break;
      case 1:
        this.currentItemId = 1;
        break;
      case 2:
        this.currentItemId = 2;
        break;
      default:
        this.currentItemId = 0;
        break;
    }
  }

  onSideNavClicked(event: NavModel) {
    this.showPage(event);
  }



}
