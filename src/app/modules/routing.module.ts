import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ApplicationComponent } from '../application/application.component';
import { PageNotFoundComponent } from '../ui/page-not-found/page-not-found.component';

const routes: Routes = [
  {
    path: '',
    component: ApplicationComponent, 
    pathMatch: 'full'
  }, {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: []
})
export class RoutingModule { }